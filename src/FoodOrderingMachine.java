import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel listTitle;
    private JButton mentaikoSpaghettiButton;
    private JButton ketchupBasedSpaghettiButton;
    private JButton vegetablesButton;
    private JButton curryButton;
    private JButton seafoodButton;
    private JButton hamburgSteakButton;
    private JPanel root2;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JLabel sum;
    private JButton mentaikoCountUp;
    private JButton mentaikoCountDown;
    private JPanel mentaikocount;
    private JLabel mentaikoCount;
    private JLabel ketchupBasedSpaghettiCount;
    private JButton ketchupBasedSpaghettiCountDown;
    private JButton ketchupBasedSpaghettiCountUp;
    private JButton vegetablesCountUp;
    private JButton vegetablesCountDown;
    private JLabel vegetablesCount;
    private JButton curryCountUp;
    private JButton curryCountDown;
    private JButton seafoodCountUp;
    private JButton seafoodCountDown;
    private JButton hamburgSteakCountUp;
    private JButton hamburgSteakCountDown;
    private JLabel curryCount;
    private JLabel seafoodCount;
    private JLabel hamburgSteakCount;
    private JButton resetButton;
    private JTabbedPane Menu;
    private JLabel colaCount;
    private JButton colaButton;
    private JButton ciderButton;
    private JButton grapeJuiceButton;
    private JPanel lunchMenu;
    private JPanel DrinkMenu;
    private JButton oolongTeaButton;
    private JButton barleyTeaButton;
    private JButton orangeJuiceButton;
    private JButton appleJuiceButton;
    private JButton gingerAleButton;
    private JButton wineButton;
    private JLabel oolongTeaCount;
    private JLabel barleyTeaCount;
    private JLabel ciderCount;
    private JLabel orangeJuiceCount;
    private JLabel appleJuiceCount;
    private JLabel grapeJuiceCount;
    private JButton colaCountUp;
    private JButton oolongTeaCountUp;
    private JButton ciderCountUp;
    private JButton orangeJuiceCountUp;
    private JButton appleJuiceCountUp;
    private JButton grapeJuiceCountUp;
    private JButton gingerAleCountUp;
    private JButton wineCountUp;
    private JButton colaCountDown;
    private JButton oolongTeaCountDown;
    private JButton barleyTeaCountDown;
    private JButton ciderCountDown;
    private JButton orangeJuiceCountDown;
    private JButton appleJuiceCountDown;
    private JButton grapeJuiceCountDown;
    private JButton gingerAleCountDown;
    private JButton wineCountDown;
    private JButton barleyTeaCountUp;
    private JLabel gingerAleCount;
    private JLabel wineCount;
    private JRadioButton colaLRadioButton;
    private JPanel colaRadioButton;
    private JPanel mentaikoRadioButton;
    private JRadioButton mentaikoMRadioButton;
    private JRadioButton mentaikoLRadioButton;
    private JRadioButton ketchupBasedSpaghettiMRadioButton;
    private JRadioButton ketchupBasedSpaghettiLRadioButton;
    private JRadioButton vegetablesMRadioButton;
    private JRadioButton vegetablesLRadioButton;
    private JRadioButton curryMRadioButton;
    private JRadioButton curryLRadioButton;
    private JRadioButton seafoodMRadioButton;
    private JRadioButton seafoodLRadioButton;
    private JRadioButton hamburgSteakMRadioButton;
    private JRadioButton hamburgSteakLRadioButton;
    private JRadioButton colaMRadioButton;
    private JRadioButton oolongTeaMRadioButton;
    private JRadioButton oolongTeaLRadioButton;
    private JRadioButton barleyTeaMRadioButton;
    private JRadioButton barleyTeaLRadioButton;
    private JRadioButton ciderMRadioButton;
    private JRadioButton ciderLRadioButton;
    private JRadioButton orangeJuiceMRadioButton;
    private JRadioButton orangeJuiceLRadioButton;
    private JRadioButton grapeJuiceLRadioButton;
    private JRadioButton grapeJuiceMRadioButton;
    private JRadioButton gingerAleMRadioButton;
    private JRadioButton gingerAleLRadioButton;
    private JRadioButton wineLRadioButton;
    private JRadioButton wineMRadioButton;
    private JRadioButton appleJuiceMRadioButton;
    private JRadioButton appleJuiceLRadioButton;
    private JRadioButton toGoRadioButton;
    private JRadioButton forHereRadioButton;
    private JLabel totalTitle;
    private JLabel yen;

    void order(String food, int price, int count, char select, boolean takeoutSelect, boolean eatinSelect) {
        if (takeoutSelect == false && eatinSelect == false) {
            JOptionPane.showMessageDialog(null,
                    "Please Select to go or for here");
        } else if ((takeoutSelect == true && eatinSelect == false) || (takeoutSelect == false && eatinSelect == true)) {
            if (count <= 0) {
                JOptionPane.showMessageDialog(null,
                        "Please Select one or more.");
                return;
            }
            if (select == 'N') {
                JOptionPane.showMessageDialog(null,
                        "Please Select Size");
                return;
            }
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to order " + food + "?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if (confirmation == 1) {
                return;
            } else {
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you for ordering " + food + "! It will be served as soon as possible.");
                    price = price * count;
                    if (takeoutSelect = true && eatinSelect == false) {
                        price = (int) (price * 1.08);
                    } else if (takeoutSelect == false && eatinSelect == true) {
                        price = (int) (price * 1.1);
                    }
                    String currentText = orderedItemsList.getText();
                    orderedItemsList.setText(currentText + food + "(" + select + ") :" + count + "\n" + price + " yen\n");
                    total += price;
                    sum.setText(total + " ");
                }
            }
        }
    }

    int total = 0;
    int mentaikoNum = 0;
    int ketchupBasedSpaghettiNum = 0;
    int vegetablesNum = 0;
    int curryNum = 0;
    int seafoodNum = 0;
    int hamburgSteakNum = 0;
    int colaNum = 0;
    int oolongTeaNum = 0;
    int barleyTeaNum = 0;
    int ciderNum = 0;
    int orangeJuiceNum = 0;
    int appleJuiceNum = 0;
    int grapeJuiceNum = 0;
    int gingerAleNum = 0;
    int wineNum = 0;
    char mentaikoSelect = 'N';
    char ketchupBasedSpaghettiSelect = 'N';
    char vegetablesSelect = 'N';
    char currySelect = 'N';
    char seafoodSelect = 'N';
    char hamburgSteakSelect = 'N';
    char colaSelect = 'N';
    char oolongTeaSelect = 'N';
    char barleyTeaSelect = 'N';
    char ciderSelect = 'N';
    char orangeJuiceSelect = 'N';
    char appleJuiceSelect = 'N';
    char grapeJuiceSelect = 'N';
    char gingerAleSelect = 'N';
    char wineSelect = 'N';
    int mentaikoPrice = 0;
    int ketchupBasedSpaghettiPrice = 0;
    int vegetablesPrice = 0;
    int curryPrice = 0;
    int seafoodPrice = 0;
    int hamburgSteakPrice = 0;
    int colaPrice = 0;
    int oolongTeaPrice = 0;
    int barleyTeaPrice = 0;
    int ciderPrice = 0;
    int orangeJuicePrice = 0;
    int appleJuicePrice = 0;
    int grapeJuicePrice = 0;
    int gingerAlePrice = 0;
    int winePrice = 0;
    boolean toGoSelect = false;
    boolean forHereSelect = false;

    public FoodOrderingMachine() {
        sum.setText(total + " ");
        mentaikoSpaghettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mentaiko Spaghetti", mentaikoPrice, mentaikoNum, mentaikoSelect, toGoSelect, forHereSelect);
            }
        });
        mentaikoSpaghettiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Mentaiko Spaghetti.png")
        ));
        ketchupBasedSpaghettiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ketchup-based Spaghetti", ketchupBasedSpaghettiPrice, ketchupBasedSpaghettiNum, ketchupBasedSpaghettiSelect, toGoSelect, forHereSelect);
            }
        });
        ketchupBasedSpaghettiButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ketchup-based Spaghetti.png")
        ));
        vegetablesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice gratin with vegetables", vegetablesPrice, vegetablesNum, vegetablesSelect, toGoSelect, forHereSelect);
            }
        });
        vegetablesButton.setIcon(new ImageIcon(
                this.getClass().getResource("Rice gratin with vegetables.png")
        ));
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice gratin with curry", curryPrice, curryNum, currySelect, toGoSelect, forHereSelect);
            }
        });
        curryButton.setIcon(new ImageIcon(
                this.getClass().getResource("Rice gratin with curry.png")
        ));
        seafoodButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice gratin with seafood", seafoodPrice, seafoodNum, seafoodSelect, toGoSelect, forHereSelect);
            }
        });
        seafoodButton.setIcon(new ImageIcon(
                this.getClass().getResource("Rice gratin with seafood.png")
        ));
        hamburgSteakButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburg steak with grated radish", hamburgSteakPrice, hamburgSteakNum, hamburgSteakSelect, toGoSelect, forHereSelect);
            }
        });
        hamburgSteakButton.setIcon(new ImageIcon(
                this.getClass().getResource("Hamburg steak with grated radish.png")
        ));
        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cola", colaPrice, colaNum, colaSelect, toGoSelect, forHereSelect);
            }
        });
        colaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Cola.png")
        ));
        oolongTeaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oolong tea", oolongTeaPrice, oolongTeaNum, oolongTeaSelect, toGoSelect, forHereSelect);

            }
        });
        oolongTeaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Oolong tea.png")
        ));
        barleyTeaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Barley tea", barleyTeaPrice, barleyTeaNum, barleyTeaSelect, toGoSelect, forHereSelect);
            }
        });
        barleyTeaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Barley tea.png")
        ));
        ciderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cider", ciderPrice, ciderNum, ciderSelect, toGoSelect, forHereSelect);
            }
        });
        ciderButton.setIcon(new ImageIcon(
                this.getClass().getResource("Cider.png")
        ));
        orangeJuiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Orange juice", orangeJuicePrice, orangeJuiceNum, orangeJuiceSelect, toGoSelect, forHereSelect);
            }
        });
        orangeJuiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Orange juice.png")
        ));
        appleJuiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Apple juice", appleJuicePrice, appleJuiceNum, appleJuiceSelect, toGoSelect, forHereSelect);
            }
        });
        appleJuiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Apple juice.png")
        ));
        grapeJuiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Grape juice", grapeJuicePrice, grapeJuiceNum, grapeJuiceSelect, toGoSelect, forHereSelect);
            }
        });
        grapeJuiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("Grape juice.png")
        ));
        gingerAleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ginger ale", gingerAlePrice, gingerAleNum, gingerAleSelect, toGoSelect, forHereSelect);
            }
        });
        gingerAleButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ginger ale.png")
        ));
        wineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Wine", winePrice, wineNum, wineSelect, toGoSelect, forHereSelect);
            }
        });
        wineButton.setIcon(new ImageIcon(
                this.getClass().getResource("Wine.png")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + total + " yen.");
                    total = 0;

                    mentaikoNum = 0;
                    ketchupBasedSpaghettiNum = 0;
                    vegetablesNum = 0;
                    curryNum = 0;
                    seafoodNum = 0;
                    hamburgSteakNum = 0;

                    colaNum = 0;
                    oolongTeaNum = 0;
                    barleyTeaNum = 0;
                    ciderNum = 0;
                    orangeJuiceNum = 0;
                    appleJuiceNum = 0;
                    grapeJuiceNum = 0;
                    gingerAleNum = 0;
                    wineNum = 0;

                    mentaikoCount.setText(String.valueOf(mentaikoNum));
                    ketchupBasedSpaghettiCount.setText(String.valueOf(ketchupBasedSpaghettiNum));
                    vegetablesCount.setText(String.valueOf(vegetablesNum));
                    curryCount.setText(String.valueOf(curryNum));
                    seafoodCount.setText(String.valueOf(seafoodNum));
                    hamburgSteakCount.setText(String.valueOf(hamburgSteakNum));

                    colaCount.setText(String.valueOf(colaNum));
                    oolongTeaCount.setText(String.valueOf(oolongTeaNum));
                    barleyTeaCount.setText(String.valueOf(barleyTeaNum));
                    ciderCount.setText(String.valueOf(ciderNum));
                    orangeJuiceCount.setText(String.valueOf(orangeJuiceNum));
                    appleJuiceCount.setText(String.valueOf(appleJuiceNum));
                    grapeJuiceCount.setText(String.valueOf(grapeJuiceNum));
                    gingerAleCount.setText(String.valueOf(gingerAleNum));
                    wineCount.setText(String.valueOf(wineNum));

                    toGoSelect = false;
                    forHereSelect = false;
                    toGoRadioButton.setEnabled(true);
                    forHereRadioButton.setEnabled(true);

                    sum.setText(total + " ");
                    orderedItemsList.setText("");
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to reset order?",
                        "Reset Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    total = 0;

                    mentaikoNum = 0;
                    ketchupBasedSpaghettiNum = 0;
                    vegetablesNum = 0;
                    curryNum = 0;
                    seafoodNum = 0;
                    hamburgSteakNum = 0;

                    colaNum = 0;
                    oolongTeaNum = 0;
                    barleyTeaNum = 0;
                    ciderNum = 0;
                    orangeJuiceNum = 0;
                    appleJuiceNum = 0;
                    grapeJuiceNum = 0;
                    gingerAleNum = 0;
                    wineNum = 0;

                    mentaikoCount.setText(String.valueOf(mentaikoNum));
                    ketchupBasedSpaghettiCount.setText(String.valueOf(ketchupBasedSpaghettiNum));
                    vegetablesCount.setText(String.valueOf(vegetablesNum));
                    curryCount.setText(String.valueOf(curryNum));
                    seafoodCount.setText(String.valueOf(seafoodNum));
                    hamburgSteakCount.setText(String.valueOf(hamburgSteakNum));

                    colaCount.setText(String.valueOf(colaNum));
                    oolongTeaCount.setText(String.valueOf(oolongTeaNum));
                    barleyTeaCount.setText(String.valueOf(barleyTeaNum));
                    ciderCount.setText(String.valueOf(ciderNum));
                    orangeJuiceCount.setText(String.valueOf(orangeJuiceNum));
                    appleJuiceCount.setText(String.valueOf(appleJuiceNum));
                    grapeJuiceCount.setText(String.valueOf(grapeJuiceNum));
                    gingerAleCount.setText(String.valueOf(gingerAleNum));
                    wineCount.setText(String.valueOf(wineNum));

                    toGoSelect = false;
                    forHereSelect = false;
                    toGoRadioButton.setEnabled(true);
                    forHereRadioButton.setEnabled(true);

                    sum.setText(total + " ");
                    orderedItemsList.setText("");
                }
            }
        });

        mentaikoCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mentaikoNum++;
                mentaikoCount.setText(String.valueOf(mentaikoNum));
            }
        });

        mentaikoCount.setText(String.valueOf(mentaikoNum));

        mentaikoCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (mentaikoNum > 0) {
                    mentaikoNum--;
                    mentaikoCount.setText(String.valueOf(mentaikoNum));
                }
            }
        });

        ketchupBasedSpaghettiCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ketchupBasedSpaghettiNum++;
                ketchupBasedSpaghettiCount.setText(String.valueOf(ketchupBasedSpaghettiNum));
            }
        });

        ketchupBasedSpaghettiCount.setText(String.valueOf(ketchupBasedSpaghettiNum));

        ketchupBasedSpaghettiCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ketchupBasedSpaghettiNum > 0) {
                    ketchupBasedSpaghettiNum--;
                    ketchupBasedSpaghettiCount.setText(String.valueOf(ketchupBasedSpaghettiNum));
                }
            }
        });

        vegetablesCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vegetablesNum++;
                vegetablesCount.setText(String.valueOf(vegetablesNum));
            }
        });

        vegetablesCount.setText(String.valueOf(vegetablesNum));

        vegetablesCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (vegetablesNum > 0) {
                    vegetablesNum--;
                    vegetablesCount.setText(String.valueOf(vegetablesNum));
                }
            }
        });

        curryCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                curryNum++;
                curryCount.setText(String.valueOf(curryNum));
            }
        });

        curryCount.setText(String.valueOf(curryNum));

        curryCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (curryNum > 0) {
                    curryNum--;
                    curryCount.setText(String.valueOf(curryNum));
                }
            }
        });

        seafoodCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seafoodNum++;
                seafoodCount.setText(String.valueOf(seafoodNum));
            }
        });

        seafoodCount.setText(String.valueOf(seafoodNum));

        seafoodCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (seafoodNum > 0) {
                    seafoodNum--;
                    seafoodCount.setText(String.valueOf(seafoodNum));
                }
            }
        });

        hamburgSteakCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hamburgSteakNum++;
                hamburgSteakCount.setText(String.valueOf(hamburgSteakNum));
            }
        });

        hamburgSteakCount.setText(String.valueOf(hamburgSteakNum));

        hamburgSteakCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (hamburgSteakNum > 0) {
                    hamburgSteakNum--;
                    hamburgSteakCount.setText(String.valueOf(hamburgSteakNum));
                }
            }
        });

        colaCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colaNum++;
                colaCount.setText(String.valueOf(colaNum));
            }
        });

        colaCount.setText(String.valueOf(colaNum));

        colaCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (colaNum > 0) {
                    colaNum--;
                    colaCount.setText(String.valueOf(colaNum));
                }
            }
        });

        oolongTeaCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                oolongTeaNum++;
                oolongTeaCount.setText(String.valueOf(oolongTeaNum));
            }
        });

        oolongTeaCount.setText(String.valueOf(oolongTeaNum));

        oolongTeaCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (oolongTeaNum > 0) {
                    oolongTeaNum--;
                    oolongTeaCount.setText(String.valueOf(oolongTeaNum));
                }
            }
        });

        barleyTeaCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                barleyTeaNum++;
                barleyTeaCount.setText(String.valueOf(barleyTeaNum));
            }
        });

        barleyTeaCount.setText(String.valueOf(barleyTeaNum));

        barleyTeaCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (barleyTeaNum > 0) {
                    barleyTeaNum--;
                    oolongTeaCount.setText(String.valueOf(barleyTeaNum));
                }
            }
        });

        ciderCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ciderNum++;
                ciderCount.setText(String.valueOf(ciderNum));
            }
        });

        ciderCount.setText(String.valueOf(ciderNum));

        ciderCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (ciderNum > 0) {
                    ciderNum--;
                    ciderCount.setText(String.valueOf(ciderNum));
                }
            }
        });

        orangeJuiceCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orangeJuiceNum++;
                orangeJuiceCount.setText(String.valueOf(orangeJuiceNum));
            }
        });

        orangeJuiceCount.setText(String.valueOf(orangeJuiceNum));

        orangeJuiceCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (orangeJuiceNum > 0) {
                    orangeJuiceNum--;
                    orangeJuiceCount.setText(String.valueOf(orangeJuiceNum));
                }
            }
        });

        appleJuiceCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appleJuiceNum++;
                appleJuiceCount.setText(String.valueOf(appleJuiceNum));
            }
        });

        appleJuiceCount.setText(String.valueOf(appleJuiceNum));

        appleJuiceCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (appleJuiceNum > 0) {
                    appleJuiceNum--;
                    appleJuiceCount.setText(String.valueOf(appleJuiceNum));
                }
            }
        });

        grapeJuiceCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                grapeJuiceNum++;
                grapeJuiceCount.setText(String.valueOf(grapeJuiceNum));
            }
        });

        grapeJuiceCount.setText(String.valueOf(grapeJuiceNum));

        grapeJuiceCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (grapeJuiceNum > 0) {
                    grapeJuiceNum--;
                    grapeJuiceCount.setText(String.valueOf(grapeJuiceNum));
                }
            }
        });

        gingerAleCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gingerAleNum++;
                gingerAleCount.setText(String.valueOf(gingerAleNum));
            }
        });

        gingerAleCount.setText(String.valueOf(gingerAleNum));

        gingerAleCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (gingerAleNum > 0) {
                    gingerAleNum--;
                    gingerAleCount.setText(String.valueOf(gingerAleNum));
                }
            }
        });

        wineCountUp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wineNum++;
                wineCount.setText(String.valueOf(wineNum));
            }
        });

        wineCount.setText(String.valueOf(wineNum));

        wineCountDown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (wineNum > 0) {
                    wineNum--;
                    wineCount.setText(String.valueOf(wineNum));
                }
            }
        });

        mentaikoMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mentaikoPrice = 500;
                mentaikoSelect = 'M';
            }
        });
        mentaikoLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mentaikoPrice = 505;
                mentaikoSelect = 'L';
            }
        });
        ketchupBasedSpaghettiMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ketchupBasedSpaghettiPrice = 540;
                ketchupBasedSpaghettiSelect = 'M';
            }
        });

        ketchupBasedSpaghettiLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ketchupBasedSpaghettiPrice = 545;
                ketchupBasedSpaghettiSelect = 'L';
            }
        });

        vegetablesMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vegetablesPrice = 480;
                vegetablesSelect = 'M';
            }
        });

        vegetablesLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                vegetablesPrice = 485;
                vegetablesSelect = 'L';
            }
        });

        curryMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                curryPrice = 450;
                currySelect = 'M';
            }
        });

        curryLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                curryPrice = 455;
                currySelect = 'L';
            }
        });

        seafoodMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seafoodPrice = 490;
                seafoodSelect = 'M';
            }
        });
        seafoodLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                seafoodPrice = 495;
                seafoodSelect = 'L';
            }
        });

        hamburgSteakMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hamburgSteakPrice = 520;
                hamburgSteakSelect = 'M';
            }
        });

        hamburgSteakLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hamburgSteakPrice = 525;
                hamburgSteakSelect = 'L';
            }
        });

        colaMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colaPrice = 190;
                colaSelect = 'M';
            }
        });

        colaLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colaPrice = 195;
                colaSelect = 'L';
            }
        });
        oolongTeaMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                oolongTeaPrice = 100;
                oolongTeaSelect = 'M';
            }
        });

        oolongTeaLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                oolongTeaPrice = 105;
                oolongTeaSelect = 'L';
            }
        });
        barleyTeaMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                barleyTeaPrice = 110;
                barleyTeaSelect = 'M';
            }
        });

        barleyTeaLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                barleyTeaPrice = 115;
                barleyTeaSelect = 'L';
            }
        });


        ciderMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ciderPrice = 180;
                ciderSelect = 'M';
            }
        });

        ciderLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ciderPrice = 185;
                ciderSelect = 'L';
            }
        });

        orangeJuiceMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orangeJuicePrice = 120;
                orangeJuiceSelect = 'M';
            }
        });

        orangeJuiceLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orangeJuicePrice = 125;
                orangeJuiceSelect = 'L';
            }
        });
        appleJuiceMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appleJuicePrice = 130;
                appleJuiceSelect = 'M';
            }
        });
        appleJuiceLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appleJuicePrice = 135;
                appleJuiceSelect = 'L';
            }
        });


        grapeJuiceMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                grapeJuicePrice = 140;
                grapeJuiceSelect = 'M';
            }
        });
        grapeJuiceLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                grapeJuicePrice = 145;
                grapeJuiceSelect = 'L';
            }
        });
        gingerAleMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gingerAlePrice = 150;
                gingerAleSelect = 'M';
            }
        });

        gingerAleLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gingerAlePrice = 155;
                gingerAleSelect = 'L';
            }
        });

        wineMRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                winePrice = 200;
                wineSelect = 'M';
            }
        });

        wineLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                winePrice = 205;
                wineSelect = 'L';
            }
        });

        toGoRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                toGoSelect = true;
                forHereSelect = false;
                forHereRadioButton.setEnabled(false);
            }
        });
        forHereRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                forHereSelect = true;
                toGoSelect = false;
                toGoRadioButton.setEnabled(false);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

